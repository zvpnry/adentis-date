import { useState } from 'react'
import { getDayString, getMonthString, getNameMonth, getWeekStartAndEndDate, addDays } from './Utils/DateUtils';
import DashboarPage from './Pages/DashboardPage';
function App() {


  const [date, setDate] = useState(getWeekStartAndEndDate(new Date()));

  const clickPrevious = () => {
    setDate(getWeekStartAndEndDate(addDays(date.startDate, -7)));
  }

  const clickNext = () => {
    setDate(getWeekStartAndEndDate(addDays(date.startDate, 7)));
  }


  return (
    <div className="App">
      <button onClick={clickPrevious}>Previous</button>
      <span>{`${getDayString(date.startDate)}-${getMonthString(date.startDate)} / ${getDayString(date.endDate)}-${getMonthString(date.endDate)}`} </span>
      <button onClick={clickNext}>Next</button>
      {getNameMonth(new Date())}
      <DashboarPage />
    </div>
  );
}

export default App;
