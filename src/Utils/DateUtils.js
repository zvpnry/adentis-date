export const months = [
    "Janeiro",
    "Feveiro",
    "Março",
    "Abril",
    "Maio",
    "Junho",
    "Julho",
    "Agosto",
    "Setembro",
    "Outubro",
    "Novembro",
    "Dezembro"
];

export const cloneDate = (date) => {
    return new Date(date);
}

export const initializeHours = (date) => {
    const newDate = cloneDate(date);
    cloneDate(date).setHours(0, 0, 0, 0);
    return newDate;
}


export const addDays = (date, days) => {
    const newDate = cloneDate(date);
    newDate.setDate(newDate.getDate() + days);
    return newDate;
}

export const getMondayDateFromActualWeek = (date) => {
    return addDays(date, -getDayOfWeek(date));

}

export const getSundayDateFromActualWeek = (date) => {
    return addDays(date, (6 - getDayOfWeek(date)));
}


export const getMonth = (date) => {
    return cloneDate(date).getMonth() + 1;
}

export const getDayOfWeek = (date) => {
    // isto é porque o 0 representa domingo e se o nosso inicio da semana seja segunda então o domingo irá representar a semana anterior
    const actualDay = cloneDate(date).getDay();
    const isSunday = actualDay === 0;
    return isSunday ? 6 : actualDay - 1;
}

export const getDay = (date) => {
    return cloneDate(date).getDate();
}

export const getMonthString = (date) => {
    return getMonth(date).toString().padStart(2, '0');
}
export const getDayString = (date) => {
    return getDay(date).toString().padStart(2, '0');
}

export const getNameMonth = (date) => {
    return months[getMonth(date) - 1];
}


export const getWeekStartAndEndDate = (date) => {
    const currentDateWithoutHours = initializeHours(date);
    const startDate = getMondayDateFromActualWeek(currentDateWithoutHours);
    const endDate = getSundayDateFromActualWeek(currentDateWithoutHours);
    return { startDate, endDate };
}





