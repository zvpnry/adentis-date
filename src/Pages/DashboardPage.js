import React, { useState } from 'react'
import TableEditable from '../Components/TableEditable/TableEditable'
import Detail from '../Components/Detail/Detail';
const DashboardPage = () => {
    const [state, setState] = useState([
        {
            id: 1,
            semana: "17-25",
            entrevistas: {
                count: 5,
                data: [{
                    nomeCandidato: "candidato 1"
                },
                {
                    nomeCandidato: "candidato 2"
                }]
            },
            prospeccao: {
                count: 4,
                data: []
            },

        }, //semana
        {
            id: 2,
            semana: "26-02",
            entrevistas: {
                count: 5,
                data: [{
                    nomeCandidato: "candidato 1"
                },
                {
                    nomeCandidato: "candidato 2"
                }]
            },
            prospeccao: {
                count: 4,
                data: []
            },

        } //semana
    ]);

    const [detail, setDetail] = useState(null);

    const showDetail = (week) => {
        setDetail(week);
    }

    const changeEntrevistaCount = (newCount, weekIndex) => {
        const newState = state.map((week, index) => {
            if (index === weekIndex) {
                const newEntrevista = { ...week.entrevistas, count: newCount };
                return { ...week, entrevistas: newEntrevista }
                //return { ...week, entrevistas: { ...week.entrevistas, count: newCount } }
            }
            return week;
        })
        setState(newState);
    }

    const onAddentrevistasDetail = (entrevistas, weekId) => {
        const newDetail = null;
        const newState = state.map(week => {
            if (week.id === weekId) {
                newDetail = { ...week, entrevistas: entrevistas };
                return { ...newDetail };
            }
            return week;
        });

        setState(newState);
        setDetail(newDetail);
    }

    return (
        <div>
            <TableEditable weeksData={state} changeEntrevistaCount={changeEntrevistaCount} showDetail={showDetail} />
            { detail && <Detail weekDetail={detail} onSbumitEntrevistas={onAddentrevistasDetail} />}

            <button onClick={() => console.log(state)}>Teste state</button>
            <button onClick={() => console.log(detail)}>Teste Detail</button>
        </div>
    )
}

export default DashboardPage
