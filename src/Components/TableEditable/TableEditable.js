import React, { useState } from 'react'

const TableEditable = ({ weeksData, changeEntrevistaCount, showDetail }) => {


    return (
        <div>
            <table>
                <thead>
                    <tr>
                        <th>Entrevista</th>
                        <th>Prospeção</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {(!weeksData || weeksData.length === 0) && <p>No results</p>}
                    {weeksData && weeksData.length > 0 && weeksData.map((week, index) => {
                        return (<tr key={index}>
                            <td><input value={week.entrevistas.count} onChange={(event) => changeEntrevistaCount(event.target.value, index)} /> {week.entrevistas.data.length}</td>
                            <td><input value={week.prospeccao.count} /> {week.prospeccao.data.length}</td>
                            <td><button onClick={() => showDetail(week)}>Mostrar detalhe</button></td>
                        </tr>)
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default TableEditable
