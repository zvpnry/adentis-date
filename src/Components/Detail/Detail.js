import React, { useState } from 'react'
import Entrevista from './Entrevista'
const Detail = ({ weekDetail, onSbumitEntrevistas }) => {
    const [isToEdit, setIsToEdit] = useState(false)

    const [expander, setExpander] = useState('');
    return (
        <div>
            <div>
                <header>Entrevistas <button onClick={() => setExpander('Entrevistas')}>Botão de expandir</button></header>
                {expander === "Entrevistas" && <Entrevista entrevistas={weekDetail.entrevistas} isEdit={isToEdit} onSubmit={onSbumitEntrevistas} weekIdentifier={weekDetail.id} />}
            </div>

            <button onClick={() => setIsToEdit(!isToEdit)}>Edit</button>
            <button onClick={() => setExpander(null)}>Botão limpar de expandir</button>
        </div>
    )
}

export default Detail
