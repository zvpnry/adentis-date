import React, { useState, useEffect } from 'react'

const Entrevista = ({ entrevistas, isEdit, onSubmit, weekIdentifier }) => {


    useEffect(() => {
        setEntrevistas(entrevistas);

    }, [entrevistas])

    const [_entrevistas, setEntrevistas] = useState(null);
    const onchange = (value, entrevistaIndex) => {
        const entrevistas = _entrevistas.data.map((entrtevista, index) => {
            if (index === entrevistaIndex) {
                return { ...entrtevista, nomeCandidato: value };
            }
            return entrtevista;
        });
        const newEntrevistas = { ..._entrevistas, data: entrevistas };
        setEntrevistas(newEntrevistas);
    }

    const renderItem = () => {
        return (
            <>
                <p>{_entrevistas.count}</p>
                <ul>

                    {_entrevistas.data.map((entrevista, index) => {
                        if (isEdit) {
                            return (<li key={index}><input value={entrevista.nomeCandidato} onChange={(ev) => onchange(ev.target.value, index)} /></li>)
                        } else {
                            return (<li>{entrevista.nomeCandidato}</li>)
                        }

                    })}
                </ul>
                {isEdit && <button onClick={() => onSubmit(_entrevistas, weekIdentifier)}>Submit</button>}
            </>
        )
    }


    return (
        <div>
            {_entrevistas && renderItem()}

        </div>
    )
}

export default Entrevista
